FROM php:5.6-apache
RUN docker-php-ext-install pdo pdo_mysql
RUN apt-get update && apt-get upgrade -y
RUN apt-get install nano -y
RUN a2enmod rewrite
RUN service apache2 restart
