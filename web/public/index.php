<?php
session_start();

require_once '../app/bootstrap.php';

$app = new Gulacsi\Teszt\core\App();

// Adattáblák létrehozása
$migrate = new Gulacsi\Teszt\database\GenerateTables();
$migrate->customers();
$migrate->logs();
$migrate->addresses();

// Táblák feltöltése kamu értékekkel
$seed = new Gulacsi\Teszt\database\SeedTables();


// TODO: Nem olyan szép megoldás, hogy így kell tiltani, hogy ne töltse fel újra
// Negációt adj hozzá vagy változtasd meg a seedDisabled értékét!!
if ($seed->seedDisabled) {
  $seed->customers();
  $seed->addresses();
}
