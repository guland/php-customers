<?php

// Autoloader betöltése
require_once 'autoload.php';


// Core osztályok betöltése, ami az MVC mintázat használatát lehetővé teszi
require_once 'core/App.php';
require_once 'core/Controller.php';


// Adatbázisok kezelésével kapcsolatos osztályok
require_once 'database/GenerateTables.php';
require_once 'database/SeedTables.php';
