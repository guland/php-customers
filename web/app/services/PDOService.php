<?php

namespace Gulacsi\Teszt\services;

require __DIR__ . '/../../vendor/autoload.php';

use Dotenv\Dotenv as Dotenv;


/**
 * Wrapper osztály a PDO használatához
 * statikus osztálypéldányt hozok létre, mivel
 * úgy döntöttem, hogy dependency injection-t nem használok
 * A döntésem oka bővebben: 'core/App.php'-ban 24. és 41. sortól induló kommentek
 */
class PDOService
{
  protected static $instance;
  protected $pdo;

  // Statikus metódus, hogy mindenhol elérhető legyen az osztálypéldány
  public static function instance()
  {
    if (self::$instance === null) {
      self::$instance = new self;
    }
    return self::$instance;
  }


  public function __construct()
  {
    // Adatbázis környezeti változók kiolvasása a .env fájlból
    $dotenv = new Dotenv($this->rdirname(__FILE__, 2));
    $dotenv->safeLoad();

    $opt  = array(
      \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
      \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
      \PDO::ATTR_EMULATE_PREPARES   => FALSE,
    );
    $dsn = 'mysql:host=' . $_ENV['DOCKERAPP_DATABASE_HOST'] .
      ';dbname=' . $_ENV['DOCKERAPP_DATABASE_NAME'] .
      ';charset=' . $_ENV['DOCKERAPP_DATABASE_CHARSET'];

    // Adatbázis kapcsolat létesítése PDO-val
    $this->pdo = new \PDO(
      $dsn,
      $_ENV['DOCKERAPP_DATABASE_USER'],
      $_ENV['DOCKERAPP_DATABASE_PASSWORD'],
      $opt
    );
  }


  // proxy natív PDO metódusokhoz
  public function __call($method, $args)
  {
    return call_user_func_array(array($this->pdo, $method), $args);
  }


  // Az előkészített kérések egyszerűbb futtatása
  public function run($sql, $args = [])
  {
    if (!$args) {
      return $this->query($sql);
    }

    // A query előkészítése itt zajlik
    $statement = $this->pdo->prepare($sql);
    $statement->execute($args);
    return $statement;
  }


  // Rekurzív mappanév-meghatározás mappaszintekkel
  private function rdirname($path, $count = 1)
  {
    if ($count > 1) {
      return dirname($this->rdirname($path, --$count));
    } else {
      return dirname($path);
    }
  }
}
