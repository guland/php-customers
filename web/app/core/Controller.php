<?php

namespace Gulacsi\Teszt\core;

use Gulacsi\Teszt\services\PDOService;

/**
 * Bővíthető kontroller alaposztály
 */
class Controller
{
  /* @var PDO */
  protected $db;

  public function __construct()
  {
    $this->db = PDOService::instance();
  }

  // A modellek számára szükséges prefix
  protected $prefixModelAutoload = "Gulacsi\Teszt\models\\";

  /**
   * Modellek betöltése, ha léteznek
   * 
   * @param mixed $model
   * 
   * @return mixed
   */
  protected function model($model)
  {
    if (file_exists('../app/models/' . $model . '.php')) {
      require_once '../app/models/' . $model . '.php';

      $model = $this->prefixModelAutoload . $model;
      return new $model();
    }
  }


  /**
   * Nézetek betöltése, ha léteznek
   * Adatokkal együtt (amennyiben vannak)
   * 
   * @param mixed $url
   * @param array $data
   * 
   * @return void
   */
  public function view($url, $data = [])
  {
    if (file_exists('../app/views/' . $url . '.php')) {
      require_once '../app/views/' . $url . '.php';
    }
  }
}
