<?php

namespace Gulacsi\Teszt\core;

/**
 * Az alkalmazás fő osztálya
 */
class App
{
  // A kontrollerekhez szükséges prefix, mivel névtereket és autoloader-t használok
  protected $prefixControllerAutoload = "Gulacsi\Teszt\controllers\\";

  // Alapértelmezett metódus
  protected $defaultMethod = 'index';

  // Alapértelmezett kontroller
  protected $defaultController = 'Home';

  protected $parameters = [];


  public function __construct()
  {
    // TODO: Nem a legelegánsabb megoldás, de a "szolgáltatásokat"
    // itt require-olni kell. Nem gondolnám, hogy milliónyi szervízre lenne szükség,
    // így szerintem elég kezelhető marad a kód, ha ide még bekerülne néhány require_once...
    require_once('../app/services/PDOService.php');


    $url = $this->processUrl();

    if (ucfirst($url[0]) === $url[0]) {
      $this->defaultController = 'Home';
    } elseif (file_exists('../app/controllers/' . ucfirst($url[0]) . '.php')) {

      // Kontroller meghatározása
      $this->defaultController = ucfirst($url[0]);
      unset($url[0]);
    }

    require_once('../app/controllers/' . $this->defaultController . '.php');
    $controllerPath = $this->prefixControllerAutoload . $this->defaultController;

    // TODO: Itt történik a kontroller példányosítása
    // esetleg itt lehetne dependency injection-nel a szolgáltatásokat átadni:
    // - use Gulacsi\Teszt\services\PDOService;
    // - new $controllerPath(PDOService $db);
    // nyilván ehhez a kontroller class-t kicsit át kellene alakítani
    // illetve itt mindenhol a require van használva, ami konzisztenssé teszi a kódot
    $this->defaultController = new $controllerPath();

    // A kontroller metódusának meghatározása (alapértelmezett: index)
    if (isset($url[1])) {
      if (method_exists($this->defaultController, $url[1])) {
        $this->defaultMethod = $url[1];
        unset($url[1]);
      }
    }

    // Az url paraméterek
    $this->parameters = $url ? array_values($url) : [];

    // A kontroller metódusának meghívása a paraméterekkel (már amennyiben vannak)
    call_user_func([$this->defaultController, $this->defaultMethod], $this->parameters);
  }


  /**
   * Az url feldolgozása, mivel ebből nyerjük ki az információt, hogy melyik
   * kontrollert kell betölteni, annak melyik metódusát kell meghívni, valamint
   * milyen opcionális paraméterek vannak az url-ben
   * 
   * @return array
   */
  private function processUrl()
  {
    if (isset($_GET['url'])) {
      return explode(
        '/',
        filter_var(rtrim($_GET['url'], '/'), FILTER_SANITIZE_URL)
      );
    }
  }
}
