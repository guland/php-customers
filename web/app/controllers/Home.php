<?php

namespace Gulacsi\Teszt\controllers;

use Gulacsi\Teszt\core\Controller;


class Home extends Controller
{

  /**
   * Főoldal
   * 
   * @return void
   */
  public function index()
  {
    $home = $this->model('Home');

    $home->title = 'Gulácsi András tesztfeladata';
    $home->description = 'A lefejlesztett alkalmazás a Vásárlók menüpontnál érhető el.';

    $this->view('home', [
      'title' => $home->title,
      'description' => $home->description
    ]);
  }
}
