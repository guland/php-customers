<?php

namespace Gulacsi\Teszt\controllers;

use Gulacsi\Teszt\core\Controller;


class Customer extends Controller
{

  /**
   * Az öszes vásárló visszaadása
   * 
   * @param array $args
   * 
   * @return void
   */
  public function index()
  {
    $pageTitle = 'Vásárlók';
    $customers = $this->db->query("SELECT * FROM customers")->fetchAll();

    $customerAddresses = $this->db->query(
      "SELECT
        c.id AS id,
        a.customer_id AS customer_id,
        a.address AS address,
        a.type AS type,
        a.is_default AS is_default,
        a.tax_number AS tax_number
      FROM customers c
      INNER JOIN addresses a ON c.id = a.customer_id"
    )->fetchAll();

    $this->view('listCustomers', [
      'title' => $pageTitle,
      'customers' => $customers,
      'customerAddresses' => $customerAddresses,
      'response' => $_SESSION['response'] ? $_SESSION['response'] : '',
    ]);
    $this->initializeSessionResponseVar();
  }



  /**
   * Egy vásárló mutatása
   * 
   * @param array $args
   * 
   * @return void
   */
  public function show($args)
  {
    // id kinyerése a lekérdezéshez
    $id = intval($args[0]);
    $sql = "SELECT * from customers WHERE id=?";
    $customer = $this->db->run($sql, [$id])->fetch();

    $sql = "SELECT
        a.id AS id,
        a.customer_id AS customer_id,
        a.address AS address,
        a.type AS type,
        a.is_default AS is_default,
        a.tax_number AS tax_number
      FROM customers c
      INNER JOIN addresses a ON c.id = a.customer_id
      WHERE c.id=?
      ";

    $customerAddresses = $this->db->run($sql, [$id])->fetchAll();

    $this->view('editCustomer', [
      'customer' => $customer,
      'customerAddresses' => $customerAddresses,
      'response' => $_SESSION['response'] ? $_SESSION['response'] : '',
    ]);
  }

  /**
   * Új vásárló hozzáadása
   * 
   * @param array $args
   * 
   * @return void
   */
  public function add()
  {
    $this->view('addCustomer', [
      'input' => $_SESSION['input'] ? $_SESSION['input'] : [],
      'response' => $_SESSION['response'] ? $_SESSION['response'] : '',
    ]);
  }



  /**
   * Új vásárló hozzáadása
   * 
   * @return void
   */
  public function create()
  {

    extract($_REQUEST);
    $inputError = false;
    $this->initializeSessionResponseVar();

    $inputs = [
      'name' => $name,
      'address' => $address,
      'email' => $email,
      'password' => $password,
      'billing_address' => $billing_address,
      'delivery_address' => $delivery_address,

    ];

    $inputError = $this->checkUserInput($inputs);
    if ($inputError) {
      $this->setSessionResponseVar('errorCreate');

      $_SESSION['input'] = [
        'name' => $name,
        'address' => $address,
        'email' => $email,
        'billing_address' => $billing_address,
        'billing_address_default' => isset($billing_address_default) ? 1 : 0,
        'tax_number' => $tax_number ? $tax_number : NULL,
        'delivery_address' => $delivery_address,
        'delivery_address_default' => isset($delivery_address_default) ? 1 : 0
      ];
      $this->addEventToLog('POST /public/customer/create', 'Sikertelen vásárló-létrehozás.');
      $this->redirect('add');
    }

    // Tisztítás
    $name = htmlentities($name);
    $address = htmlentities($address);
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    $password = md5($password);
    $billing_address = htmlentities($billing_address);
    $delivery_address = htmlentities($delivery_address);
    $billing_address_default = isset($billing_address_default) ? 1 : 0;
    $delivery_address_default = isset($delivery_address_default) ? 1 : 0;

    $sql = "INSERT INTO customers
            (name, address, password, email)
            VALUES (?,?,?,?)";

    // a run metóduson belül történik az előkészítés
    $this->db->run($sql, [$name, $address, $password, $email]);
    $customer_id = intval($this->db->lastInsertId());

    // Számlázási cím
    $sql = "INSERT INTO addresses
    (customer_id, address, type, is_default, tax_number)
    VALUES (?,?,?,?,?)";
    $this->db->run($sql, [$customer_id, $billing_address, 'billing', $billing_address_default, $tax_number]);

    // Szállítási cím
    $sql = "INSERT INTO addresses
    (customer_id, address, type, is_default, tax_number)
    VALUES (?,?,?,?,?)";
    $this->db->run($sql, [$customer_id, $delivery_address, 'delivery', $delivery_address_default, NULL]);


    $this->setSessionResponseVar('successCreate');
    $this->addEventToLog('POST /public/customer/create', 'Sikeres vásárló-létrehozás.');

    $this->redirect('index');
  }



  /**
   * Egy létező vásárló módosítása
   * 
   * @param array $args
   * 
   * @return void
   */
  public function update($args)
  {

    $id = intval($args[0]);
    extract($_REQUEST);

    $inputError = false;
    $this->initializeSessionResponseVar();

    $inputs = [
      'name' => $name,
      'address' => $address,
      'email' => $email,
      'password' => $password,
    ];

    $inputError = $this->checkUserInput($inputs);

    if ($inputError) {
      $this->setSessionResponseVar('errorUpdate');
      $this->addEventToLog(('PUT /public/customer/update/' . $id), 'A vásárló adatainak módosítása sikertelen.');

      $this->redirect('show', $id);
    }

    // Tisztítás
    $name = htmlentities($name);
    $address = htmlentities($address);
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    $password = md5($password);

    $sql = "UPDATE customers
            SET name=?, address=?, password=?, email=?
            WHERE id=?";

    // a run metóduson belül történik az előkészítés
    $this->db->run($sql, [$name, $address, $password, $email, $id]);

    $this->setSessionResponseVar('successUpdate');
    $this->addEventToLog(('PUT /public/customer/update/' . $id), 'A vásárló adatai sikeresen módosítva.');

    $this->redirect('show', $id);
  }



  /**
   * Egy vásárló törlése
   * 
   * @param mixed $args
   * 
   * @return [type]
   */
  public function delete($args)
  {
    $this->setSessionResponseVar('errorDeleteOne');

    $id = intval($args[0]);

    $sql = "DELETE FROM customers WHERE id=?";

    $this->db->run($sql, [$id]);

    $this->setSessionResponseVar('successDeleteOne');

    $hasError = $_SESSION['response'] === 'errorDeleteOne';

    $this->addEventToLog(
      ('DELETE /public/customer/delete/' . $id),
      '',
      $hasError,
      'A vásárló törlése sikertelen.',
      'Sikeres vásárló törlés.'
    );

    $this->redirect('index');
  }


  /**
   * Összes vásárló törlése
   * @return void
   */
  public function truncate()
  {
    $this->setSessionResponseVar('errorDeleteAll');

    // törölni kell truncate előtt a constraint-et
    $sql = "ALTER TABLE addresses
            DROP FOREIGN KEY fk_customer";
    $this->db->run($sql);

    $sql = "TRUNCATE TABLE customers";
    $this->db->run($sql);

    // érvénytelen id-kat tartalmaz, ürítendő
    $sql = "TRUNCATE TABLE addresses";
    $this->db->run($sql);

    // újra hozzá kell adni a constraint-et
    $sql = "ALTER TABLE addresses
            ADD CONSTRAINT fk_customer
            FOREIGN KEY (customer_id)
            REFERENCES customers (id)
            ON DELETE CASCADE
      ";
    $this->db->run($sql);

    $this->setSessionResponseVar('successDeleteAll');

    $hasError = $_SESSION['response'] === 'errorDeleteAll';

    $this->addEventToLog(
      ('DELETE /public/customer/truncate'),
      '',
      $hasError,
      'Az összes vásárló törlése sikertelen.',
      'Az összes vásárló sikeresen törölve.'
    );


    $this->redirect('index');
  }



  /* --------------------------------------------------------------
     TODO: Egyéb segédfüggvények -> a modellbe lehetne tenni ezeket */

  /**
   * Visszairányítás hibás / sikeres input esetén
   * 
   * @param boolean $error
   * 
   * @return void
   */
  private function redirect($page, $id = '')
  {
    $path = '/public/customer/' . $page . ($id ?  ('/'  . $id) : '');
    header('Location: ' . $path);
    die();
  }


  /**
   * A felhasználói input ellenőrzése
   * 
   * @param array $inputs
   * 
   * @return boolean
   */
  private function checkUserInput($inputs)
  {
    $err = false;
    foreach ($inputs as $key => $value) {
      if (!isset($value) || empty($value)) {
        $err = true;
      }
    }
    return $err;
  }

  private function initializeSessionResponseVar()
  {
    $_SESSION['response'] = '';
  }

  private function setSessionResponseVar($value, $var = 'response')
  {
    $_SESSION[$var] = $value;
  }

  /**
   * Az események loggolása
   * 
   * @param string $endpoint
   * @param string $message
   * @param null $hasError
   * @param string $errorMessage
   * @param string $successMessage
   * 
   * @return void
   */
  private function addEventToLog($endpoint, $message = '', $hasError = null, $errorMessage = '', $successMessage = '')
  {
    if ($hasError === null) {
      $sql = "INSERT INTO logs (endpoint, message) VALUES(?,?)";
      $this->db->run($sql, [$endpoint, $message]);
    } else {
      $sql = "INSERT INTO logs (endpoint, message) VALUES(?,?)";
      $this->db->run($sql, [$endpoint, ($hasError === true) ? $errorMessage : $successMessage]);
    }
  }
}
