<?php extract($data); ?>


<html lang="hu">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?php echo $customer['Name'] ?></title>

  <link rel="stylesheet" href="https://unpkg.com/purecss@2.0.6/build/pure-min.css" integrity="sha384-Uu6IeWbM+gzNVXJcM9XV3SohHtmWE+3VGi496jvgX1jyvDTXfdK+rfZc8C1Aehk5" crossorigin="anonymous">
  <link rel="stylesheet" href="https://unpkg.com/purecss@2.0.6/build/grids-responsive-min.css" />
  <link rel="stylesheet" href="/../../assets/style.css">
</head>

<body>
  <header>
    <div class="pure-menu pure-menu-horizontal">
      <a href="#" class="pure-menu-heading pure-menu-link">TESZTAPP</a>
      <ul class="pure-menu-list">
        <li class="pure-menu-item">
          <a href="/public/" class="pure-menu-link">Kezdőlap</a>
        </li>
        <li class="pure-menu-item">
          <a href="/public/customer/index" class="pure-menu-link">Vásárlók</a>
        </li>
      </ul>
    </div>
  </header>

  <div class="container">
    <h1><?php echo $customer['name'] ?></h1>

    <form style="max-width: 1200px;" action="<?php echo '/public/customer/update/' . $customer['id']; ?>" method="PUT" class="pure-form pure-form-stacked">
      <fieldset>
        <legend>Adatok módosítása</legend>

        <div class="notification notification-error <?php echo $response === 'errorUpdate' ? 'show' : 'hide';  ?>"><?php echo 'Az összes mező kitöltése kötelező.'; ?></div>
        <div class="notification notification-success <?php echo $response === 'successUpdate' ? 'show' : 'hide';  ?>"><?php echo 'Az adatok sikeresen módosítva.'; ?></div>

        <div class="pure-g">
          <div class="pure-u-1 pure-u-md-1-2">
            <div>
              <label for="name">Név</label>
              <input class="pure-u-23-24" type="text" name="name" value="<?php echo $customer['name'] ?>">
            </div>

            <div>
              <label for="address">Lakcím</label>
              <input class="pure-u-23-24" type="text" name="address" value="<?php echo $customer['address'] ?>">
            </div>

            <div>
              <label for="email">E-mail</label>
              <input class="pure-u-23-24" type="email" name="email" value="<?php echo $customer['email'] ?>">
            </div>

            <div>
              <label for="password">Jelszó</label>
              <input class="pure-u-23-24" type="password" name="password">
            </div>
          </div>

          <div class="pure-u-1 pure-u-md-1-2">

            <?php foreach ($customerAddresses as $address) {
              if ($address['type'] == 'billing') { ?>

                <fieldset>
                  <div>
                    <label for="billing_address">Számlázási cím* <input type="checkbox" name="billing_address_default<?php echo $address['id']; ?>" <?php echo $address['is_default'] ? 'checked' : ''; ?>> <small>Alapértelmezett</small></label>
                    <input type="text" name="billing_address<?php echo $address['id']; ?>" class="pure-u-23-24" value="<?php echo $address['address'] ? $address['address'] : ''; ?>">

                    <label for="tax_number">Adószám (opcionális)</label>
                    <input class="pure-u-23-24" type="text" name="tax_number<?php echo $address['id']; ?>" value="<?php echo $address['tax_number'] ? $address['tax_number'] : ''; ?>">
                  </div>
                </fieldset>


            <?php }
            } ?>

            <?php foreach ($customerAddresses as $address) {
              if ($address['type'] == 'delivery') { ?>

                <fieldset>
                  <div>
                    <label for="delivery_address">Szállítási cím* <input type="checkbox" name="delivery_address_default<?php echo $address['id']; ?>" <?php echo $address['is_default'] ? 'checked' : ''; ?>> <small>Alapértelmezett</small></label>
                    <input class="pure-u-23-24" type="text" name="delivery_address<?php echo $address['id']; ?>" class="pure-input-1-2" value="<?php echo $address['address'] ? $address['address'] : ''; ?>">
                  </div>
                </fieldset>

            <?php }
            } ?>

          </div>

        </div>
        <button type="submit" class="pure-button pure-button-primary button-large mr-10"><i class="fas fa-save"></i> Módosítások mentése</button>
        <a class="pure-button pure-button-secondary button-large" href="/public/customer/index">Vissza</a>
      </fieldset>
    </form>
  </div>

</body>

</html>