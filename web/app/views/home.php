<html lang="hu">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?php echo $data['title'] ?></title>

  <link rel="stylesheet" href="https://unpkg.com/purecss@2.0.6/build/pure-min.css" type="text/css" integrity="sha384-Uu6IeWbM+gzNVXJcM9XV3SohHtmWE+3VGi496jvgX1jyvDTXfdK+rfZc8C1Aehk5" crossorigin="anonymous">
  <link rel="stylesheet" href="/../../assets/style.css" type="text/css">
</head>

<body>
  <header>
    <div class="pure-menu pure-menu-horizontal">
      <a href="#" class="pure-menu-heading pure-menu-link">Gulácsi András</a>
      <ul class="pure-menu-list">
        <li class="pure-menu-item pure-menu-selected">
          <a href="/public/" class="pure-menu-link">Kezdőlap</a>
        </li>
        <li class="pure-menu-item">
          <a href="/public/customer/index" class="pure-menu-link">Vásárlók</a>
        </li>
      </ul>
    </div>
  </header>

  <div class="container">
    <h1><?php echo $data['title'] ?></h1>
    <p><?php echo $data['description'] ?></p>
    <a class="pure-button pure-button-primary button-large" href="/public/customer/index">
      Vásárlók
    </a>
  </div>

</body>

</html>