<?php extract($data); ?>


<html lang="hu">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?php echo $customer['Name'] ?></title>

  <link rel="stylesheet" href="https://unpkg.com/purecss@2.0.6/build/pure-min.css" integrity="sha384-Uu6IeWbM+gzNVXJcM9XV3SohHtmWE+3VGi496jvgX1jyvDTXfdK+rfZc8C1Aehk5" crossorigin="anonymous">
  <link rel="stylesheet" href="https://unpkg.com/purecss@2.0.6/build/grids-responsive-min.css" />
  <link rel="stylesheet" href="/../../assets/style.css">
</head>

<body>
  <header>
    <div class="pure-menu pure-menu-horizontal">
      <a href="#" class="pure-menu-heading pure-menu-link">TESZTAPP</a>
      <ul class="pure-menu-list">
        <li class="pure-menu-item">
          <a href="/public/" class="pure-menu-link">Kezdőlap</a>
        </li>
        <li class="pure-menu-item">
          <a href="/public/customer/index" class="pure-menu-link">Vásárlók</a>
        </li>
      </ul>
    </div>
  </header>

  <div class="container">
    <h1>Új vásárló hozzáadása</h1>

    <form style="max-width: 1200px;" action="/public/customer/create" method="POST" class="pure-form pure-form-stacked">
      <fieldset>
        <legend>Vásárló hozzáadása</legend>

        <div class="notification notification-error <?php echo $response === 'errorCreate' ? 'show' : 'hide';  ?>"><?php echo 'Az összes mező kitöltése kötelező.'; ?></div>

        <div class="pure-g">
          <div class="pure-u-1 pure-u-md-1-2">
            <div>
              <label for="name">Név*</label>
              <input class="pure-u-23-24" type="text" name="name" value="<?php echo $input['name'] ? $input['name'] : ''; ?>">
            </div>

            <div>
              <label for="address">Lakcím*</label>
              <input class="pure-u-23-24" type="text" name="address" value="<?php echo $input['address'] ? $input['address'] : ''; ?>">
            </div>

            <div>
              <label for="email">E-mail*</label>
              <input class="pure-u-23-24" type="email" name="email" value="<?php echo $input['email'] ? $input['email'] : ''; ?>">
            </div>

            <div>
              <label for="password">Jelszó*</label>
              <input class="pure-u-23-24" type="password" name="password">
            </div>
          </div>

          <div class="pure-u-1 pure-u-md-1-2">
            <fieldset>
              <div>
                <label for="billing_address">Számlázási cím* <input type="checkbox" name="billing_address_default" <?php echo $input['billing_address_default'] ? 'checked' : ''; ?>> <small>Alapértelmezett</small></label>
                <input type="text" name="billing_address" class="pure-u-23-24" value="<?php echo $input['billing_address'] ? $input['billing_address'] : ''; ?>">

                <label for="tax_number">Adószám (opcionális)</label>
                <input class="pure-u-23-24" type="text" name="tax_number" value="<?php echo $input['tax_number'] ? $input['tax_number'] : ''; ?>">
              </div>
            </fieldset>

            <fieldset>
              <div>
                <label for="delivery_address">Szállítási cím* <input type="checkbox" name="delivery_address_default" <?php echo $input['delivery_address_default'] ? 'checked' : ''; ?>> <small>Alapértelmezett</small></label>
                <input class="pure-u-23-24" type="text" name="delivery_address" class="pure-input-1-2" value="<?php echo $input['delivery_address'] ? $input['delivery_address'] : ''; ?>">
              </div>
            </fieldset>
          </div>

        </div>

        <button class="pure-button pure-button-primary button-large mr-10" type="submit">
          <i class="fas fa-plus"></i>
          Új hozzáadása
        </button>
        <a class="pure-button pure-button-secondary button-large" href="/public/customer/index">Vissza</a>
        <fieldset>
    </form>

  </div>

</body>

</html>