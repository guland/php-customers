<?php extract($data); ?>

<html lang="hu">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Vásárlók</title>

  <link rel="stylesheet" href="https://unpkg.com/purecss@2.0.6/build/pure-min.css" integrity="sha384-Uu6IeWbM+gzNVXJcM9XV3SohHtmWE+3VGi496jvgX1jyvDTXfdK+rfZc8C1Aehk5" crossorigin="anonymous">
  <link rel="stylesheet" href="/../../assets/style.css">
  <link rel="stylesheet" href="/../../node_modules/@fortawesome/fontawesome-free/css/all.min.css">

</head>

<body>
  <header>
    <div class="pure-menu pure-menu-horizontal">
      <a href="#" class="pure-menu-heading pure-menu-link">TESZTAPP</a>
      <ul class="pure-menu-list">
        <li class="pure-menu-item">
          <a href="/public/" class="pure-menu-link">Kezdőlap</a>
        </li>
        <li class="pure-menu-item pure-menu-selected">
          <a href="/public/customer/index" class="pure-menu-link">Vásárlók</a>
        </li>
      </ul>
    </div>
  </header>

  <div class="container">
    <h1>Vásárlók</h1>
    <div class="notification notification-success <?php echo $response === 'successCreate' ? 'show' : 'hide';  ?>"><?php echo 'Vásárló sikeresen hozzáadva.'; ?></div>

    <div class="notification notification-error <?php echo $response === 'errorDeleteAll' ? 'show' : 'hide';  ?>"><?php echo 'Nem sikerült az összes vásárló törlése.'; ?></div>
    <div class="notification notification-success <?php echo $response === 'successDeleteAll' ? 'show' : 'hide';  ?>"><?php echo 'Az összes vásárló sikeresen törölve.'; ?></div>

    <div class="notification notification-error <?php echo $response === 'errorDeleteOne' ? 'show' : 'hide';  ?>"><?php echo 'Nem sikerült az egy vásárló törlése.'; ?></div>
    <div class="notification notification-success <?php echo $response === 'successDeleteOne' ? 'show' : 'hide';  ?>"><?php echo 'Egy vásárló sikeresen törölve.'; ?></div>

    <a class="pure-button pure-button-primary mb-30" href="/public/customer/add">
      <i class="fas fa-plus"></i>
      Új hozzáadása
    </a>

    <ul class="customer-grid">
      <?php
      foreach ($customers as $customer) {
        $li = '<li class="card">';
        $li .= '<h2 class="card-title">' . $customer['name'] . '</h2>';

        $li .= '<div class="button-bar">';
        $li .= '<a class="pure-button pure-button-primary button-small mr-10" href="/public/customer/show/' . $customer['id'] .  '"><i class="fas fa-pen"></i>Módosítás</a>';
        $li .= '<a class="pure-button button-error button-small" href="/public/customer/delete/' . $customer['id'] .  '"><i class="fas fa-user-minus"></i> Törlés</a>';
        $li .= '</div>';

        $li .= '<address>' . $customer['address'] . '</address>';
        $li .= '<p>' . $customer['email'] . '</p>';
        $li .= '<small class="password-hash">' . $customer['password'] . '</small>';

        $li .= '<hr style="color:#eee;">';

        $li .= '<h5>Számlázási cím(ek):</h5>';

        $billingCount = 0;
        foreach ($customerAddresses as $address) {
          if ($address['customer_id'] == $customer['id'] && $address['type'] == 'billing') {
            $billingCount++;
            $li .= '<div class="mb-10">';
            $li .= '<address>' . $address['address'] . '</address>' . ($address['is_default'] == 1 ? '<small class="gray-50">(Alapértelmezett)</small>' : '');

            if ($address['tax_number'] != null) {
              $li .= '<br><small class="gray-50">(Adószám: ' . $address['tax_number'] . ')</small>';
            }
            $li .= '</div>';
          }
        }
        if ($billingCount === 0) {
          $li .= 'nincs megadva';
        }

        $li .= '<h5>Szállítási cím(ek):</h5>';
        $deliveryCount = 0;
        foreach ($customerAddresses as $address) {
          if ($address['customer_id'] == $customer['id'] && $address['type'] === 'delivery') {
            $li .= '<div class="mb-10">';
            $li .= '<address>' . $address['address'] . '</address>' . ($address['is_default'] == 1 ? '<small class="gray-50">(Alapértelmezett)</small>' : '');
            $li .= '</div>';

            $deliveryCount++;
          }
        }
        if ($deliveryCount === 0) {
          $li .= 'nincs megadva';
        }

        $li .= '</li>';

        echo $li;
      }
      ?>
    </ul>

    <form class="delete-all-customers-form" action="/public/customer/truncate" method="DELETE">
      <button class="pure-button button-error button-large" type="submit">
        <i class="fas fa-trash"></i>
        Összes törlése
      </button>
    </form>

  </div>

</body>

</html>