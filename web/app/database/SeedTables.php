<?php

namespace Gulacsi\Teszt\database;

use Gulacsi\Teszt\services\PDOService;

require_once '../vendor/autoload.php';

class SeedTables
{
  // PDO wrapper
  protected $db;

  // A feltöltés letiltására
  public $seedDisabled = true;

  // Faker
  protected $faker;


  public function __construct()
  {
    $this->db = PDOService::instance();
    $this->faker = \Faker\Factory::create();
  }


  /**
   * Vásárlók tábla feltöltése kamu adatokkal
   * 
   * @return void
   */
  public function customers()
  {
    $max = 5;
    $data = [];

    for ($i = 0; $i < $max; $i++) {
      array_push($data, [
        $this->faker->name,
        $this->faker->address,
        // md5: nem biztonságos hash, de itt most ennek nincs jelentősége
        md5($this->faker->password),
        $this->faker->email
      ]);
    }

    $statement = $this->db->prepare("INSERT INTO customers (name, address, password, email) VALUES (?,?,?,?)");
    try {
      $this->db->beginTransaction();
      foreach ($data as $row) {
        $statement->execute($row);
      }
      $this->db->commit();
    } catch (\Exception $e) {
      $this->db->rollback();
      throw $e;
    }
  }


  /**
   * Vásárlók tábla feltöltése kamu adatokkal
   * 
   * @return void
   */
  public function addresses()
  {
    $max = 10;
    $data = [];
    $type = ['billing', 'delivery'];

    for ($i = 0; $i < $max; $i++) {
      array_push($data, [
        rand(1, 5),
        $this->faker->address,
        $type[array_rand($type)],
        ($i === $max - 1) ? 1 : 0,
        NULL
      ]);
    }

    $statement = $this->db->prepare("INSERT INTO addresses (customer_id, address, type, is_default, tax_number) VALUES (?,?,?,?,?)");
    try {
      $this->db->beginTransaction();
      foreach ($data as $row) {
        $statement->execute($row);
      }
      $this->db->commit();
    } catch (\Exception $e) {
      $this->db->rollback();
      throw $e;
    }
  }
}
