<?php

namespace Gulacsi\Teszt\database;

use Gulacsi\Teszt\services\PDOService;

class GenerateTables
{

  protected $db;

  public function __construct()
  {
    $this->db = PDOService::instance();
  }


  /**
   * Tábla létrehozása a vásárlóknak, ha még nem létezik
   * 
   * @return void
   */
  public function customers()
  {

    $sql = "CREATE TABLE IF NOT EXISTS customers(
    id INT (11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR (255) NOT NULL,
    address VARCHAR (255) NOT NULL,
    password VARCHAR (255) NOT NULL,
    email VARCHAR (255) NOT NULL) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci; ";

    $this->db->exec($sql);
  }


  /**
   * Tábla létrehozása a loggoláshoz, ha még nem létezik
   * 
   * @return void
   */
  public function logs()
  {

    $sql = "CREATE TABLE IF NOT EXISTS logs(
    id INT (11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    endpoint VARCHAR (255) NOT NULL,
    message VARCHAR (255) NOT NULL,
    date DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci; ";

    $this->db->exec($sql);
  }


  /**
   * Tábla létrehozása a szállítási és a számlázási címeknek
   * Minden vásárlóhoz tartozhat több cím, kapcsolótáblára nincs itt szükség
   * 
   * @return void
   */
  public function addresses()
  {
    $sql = "CREATE TABLE IF NOT EXISTS addresses(
    id INT (11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    customer_id INT (11) NULL,
    address VARCHAR (255) NOT NULL,
    type ENUM('billing', 'delivery') NOT NULL,
    is_default TINYINT DEFAULT 0 NOT NULL,
    tax_number VARCHAR (60) NULL,
    CONSTRAINT fk_customer
      FOREIGN KEY (customer_id)
      REFERENCES customers (id)
      ON DELETE CASCADE) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci; ";

    $this->db->exec($sql);
  }
}
