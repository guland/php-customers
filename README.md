# Vásárlók PHP app

## Instrukciók

1. Az .env fájlban van az összes konfiguráció (beleértve a jelszavakat). Nem muszáj változtatni az értékeken.
2. A composer csomagok telepítése szükséges (dotenv, faker): `composer require`
3. A /web mappában van a forrás. Szükséges az fontawesome ikonkészlet telepítése: `npm i`
4. A projekt belövése:

```
set -a
source .env
sudo docker-compose up --build
```

Az app elérhetősége: http://localhost:8000/public

Az adattáblák automatikusan létrejönnek, ha a fenti url betöltésre kerül. A seedelés is megvalósul. De először csak egyszer nyisd meg az url-t, mert
a seeder folyamatosan tölti fel az adattáblákat. Ezért FONTOS: `/web/public/index.php` 19. sorában adj hozzá egy negálást a feltételhez: `if ($seed->seedDisabled) {` vagy változtasd meg a `seedDisabled` értékét `false`-ra.

Sajnos nem volt elég időm befejezni a feladatot, mert sokáig halogattam.
Ami hiányzik: a számlázási / szállítási címek módosítása a vásárlóknál (a többi adat módosítható, csak az említett címek nem frissülnek).
Minden más működik.

- PDO-t használtam, OOP, MVC
- php5.6, mysql5.6,apache2,phpmyadmin
- log: adattáblába kerülnek loggolásra az események
- frontend reszponzív
- nem használtam keretrendszereket (a "pure css" csak egy kis library, amit használtam)

Még sokkat lehet alakítani a munkámon, de ennyire volt időm. Egy JSON API-t is kialakíthattam volna, és JavaScripttel csináltam volna meg a frontend-et.
A validáció / tisztítás bővíthető lenne még.
